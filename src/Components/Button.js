import React from 'react'

function Button(props) {

  const { func, text } = props;
  return (
    <>
        <button onClick={func}>{text}</button>
    </>
  )
}

export default Button