import React,{useState,useEffect} from 'react'
import Button from './Button';

function StopWatch() {
    const [count,setCount] = useState(0);
    const [active,setActive] = useState(false);

    const tick = () => {
        setCount(count + 1)
    }


   useEffect(()=>{
        if(active === true){
            const timer = setInterval(tick,10);
            return () => {
                clearInterval(timer);
            }
        }
    })

    const handlelaps = () =>{
        let listelement = document.createElement('li');
        listelement.innerHTML = count;
        listelement.style = 'color : red';
        let list = document.getElementById('list');
        list.appendChild(listelement);
    }

    const reset = () => {
        if(active === false) {
            setCount(0) ;
            let list = document.getElementById('list');
            list.innerHTML = "";
        } 
    }



    return (
        <>
            <h3 id='root'>{count}</h3>
            <Button func={()=>setActive(true)} text="START" />
            <Button func={()=>setActive(false)} text="STOP"/>
            <Button func={reset} text="RESET"/>
            <Button func={handlelaps} text="LAPS"/>
            <ol id='list'></ol>
        </>
    )
}
export default StopWatch