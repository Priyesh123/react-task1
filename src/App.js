import './App.css';
import StopWatch from './Components/StopWatch';

function App() {
  return (
    <div className="App">
      <h1>React StopWatch</h1>
      <StopWatch/>
    </div>
  );
}

export default App;
